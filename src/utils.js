import AsyncStorage from "@react-native-community/async-storage";

async function getKeyFromStorage(key) {
  let req_data = await AsyncStorage.getItem(`${key}`);
  return req_data;
}

export default getKeyFromStorage;
