import React, { createContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  KeyboardAvoidingView,
  BackHandler
} from "react-native";
import { Avatar, Portal, TextInput } from "react-native-paper";
import { firestore, storage } from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";
import { Feed } from "../Feed";
import Comments from "../Comments";
import Searchbar from "../SearchBar";
const { height, width } = Dimensions.get("screen");

export const CommentContext = createContext(null);

class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: "",
      profile_pic: "",
      selected_post: "",
      menuvisible: false
    };
    this.db = firestore().collection("users");
    this._didFocusSubscription = props.navigation.addListener(
      "didFocus",
      payload =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  async componentDidMount() {
    const phone = await AsyncStorage.getItem("TOKEN");
    this.setState(
      {
        phoneNumber: phone
      },
      () => {
        const query = this.db.doc(this.state.phoneNumber);
        query.onSnapshot(snapshot => {
          if (snapshot.exists) {
            const data = snapshot.data();
            this.setState(
              {
                user: data
              },
              () => {
                if (data.name) {
                  if (data.profile_pic) {
                    storage()
                      .ref(`${this.state.phoneNumber}/profile.jpg`)
                      .getDownloadURL()
                      .then(url => {
                        this.setState({
                          profile_pic: url
                        });
                      });
                  }
                } else {
                  this.props.navigation.navigate("ProfileForm");
                }
              }
            );
          }
        });
      }
    );
    this._willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      payload =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    if (this.state.visible) {
      this.closeModal();
      return true;
    } else {
      return false;
    }
  };

  openModal = id => {
    console.log(id);
    this.setState({
      selected_post: id,
      visible: true
    });
  };

  closeModal = () => {
    this.setState({
      selected_post: "",
      visible: false
    });
  };

  render() {
    const { user, profile_pic } = this.state;
    return (
      <>
        {this.state.visible ? (
          <Portal>
            <KeyboardAvoidingView style={{ flex: 1 }}>
              <Comments
                data={this.state}
                visible={this.state.visible}
                post_id={this.state.selected_post}
                handleClose={() => this.closeModal()}
              />
            </KeyboardAvoidingView>
          </Portal>
        ) : null}

        <Feed openModal={this.openModal}>
          <View style={styles.container}>
            <Searchbar
              user={this.state.user}
              profile_pic={this.state.profile_pic}
            />

            <View>
              {this.state.profile_pic ? (
                <Avatar.Image
                  size={180}
                  style={{ marginBottom: -150 }}
                  source={{ uri: profile_pic }}
                />
              ) : (
                <Avatar.Icon size={200} style={styles.user} icon="person" />
              )}
            </View>
            <View style={styles.info}>
              <Text style={styles.title}>{user ? user.name : ""}</Text>
              <Text style={styles.sub}>{user ? user.bio : ""}</Text>
            </View>
            <View style={styles.row}>
              <View style={{ marginHorizontal: 12 }}>
                <TextInput
                  placeholder="Write something on your mind"
                  mode="outlined"
                  multiline={true}
                  numberOfLines={3}
                  style={{ width: "100%" }}
                  onTouchStart={() =>
                    this.props.navigation.navigate("CreatePost", { user })
                  }
                />
              </View>
            </View>
          </View>
        </Feed>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center"
  },
  image: {
    flex: 2,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  row: {
    flex: 1,
    width: "100%"
  },
  user: {
    marginBottom: -150,
    borderWidth: 2,
    borderColor: "white"
  },
  info: {
    flex: 2,
    justifyContent: "flex-end"
  },
  title: {
    marginTop: 20,
    fontSize: 24,
    textAlign: "center",
    fontWeight: "bold"
  },
  sub: {
    fontSize: 16,
    margin: 8,
    textAlign: "center"
  }
});

export default ProfilePage;
