import React, { useState, useEffect } from "react";
import { ScrollView, View, Text, StyleSheet } from "react-native";
import { Button, TextInput, Avatar, Appbar } from "react-native-paper";
import TextInputMask from "react-native-text-input-mask";
import { firestore, storage } from "react-native-firebase";
import ImagePicker from "react-native-image-picker";
import AsyncStorage from "@react-native-community/async-storage";

const ProfileForm = props => {
  const db = firestore().collection("users");
  let token;
  const storageRef = storage().ref();
  const [inputs, setInput] = useState({
    name: "",
    bio: "",
    dob: "",
    image_data: null
  });
  const [isEdit, setIsEdit] = useState(false);

  useEffect(() => {
    const userProps = props.navigation.getParam("user", "");
    const profileProp = props.navigation.getParam("profile_pic", "");
    if (userProps) {
      setInput(userProps);
      setIsEdit(true);
    }
    if (profileProp) {
      handleInput("image_data", profileProp);
    }
  }, [props]);

  function handleInput(target, value) {
    setInput(state => {
      return { ...state, [target]: value };
    });
  }

  function pickImage() {
    ImagePicker.showImagePicker(undefined, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = response.uri;
        const data = "data:image/jpeg;base64," + response.data;

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        console.log(source, data);
        setInput(state => ({
          ...state,
          image_data: source
        }));
      }
    });
  }

  function createProfile() {
    const { name, bio, dob, image_data } = inputs;
    AsyncStorage.getItem("TOKEN")
      .then(token => {
        let profile_pic = `${token}/profile.jpg`;
        if (name && bio && dob) {
          db.doc(token)
            .set(
              {
                name,
                bio,
                dob
              },
              { merge: true }
            )
            .then(() => {
              if (image_data) {
                const profileRef = storageRef.child(profile_pic);
                profileRef
                  .putFile(inputs.image_data)
                  .then(snapshot => {
                    db.doc(token)
                      .set(
                        {
                          profile_pic: snapshot.downloadURL
                        },
                        { merge: true }
                      )
                      .then(() => props.navigation.goBack());
                  })
                  .catch(err => console.log(err));
              } else {
                props.navigation.goBack();
              }
            });
        }
      })
      .catch(err => console.log(err));
  }

  return (
    <>
      <Appbar.Header
        style={{
          backgroundColor: "white"
        }}
      >
        <Appbar.BackAction onPress={() => props.navigation.goBack()} />
        <Appbar.Content title={isEdit ? "Edit Profile" : "Create Profile"} />
      </Appbar.Header>
      <ScrollView contentContainerstyle={styles.container}>
        <TextInput
          mode="outlined"
          style={styles.input}
          value={inputs.name}
          label="Your Name"
          onChangeText={text => handleInput("name", text)}
        />
        <TextInput
          mode="outlined"
          style={styles.input}
          value={inputs.bio}
          label="Your Bio"
          onChangeText={text => handleInput("bio", text)}
        />
        <TextInput
          label="Date of Birth"
          mode="outlined"
          value={inputs.dob}
          style={styles.input}
          placeholder="DD-MM-YYYY"
          render={props => (
            <TextInputMask {...props} mask={"[00]-[00]-[0000]"} />
          )}
          onChangeText={text => handleInput("dob", text)}
        />

        {inputs.image_data && (
          <View style={{ alignItems: "center" }}>
            <Avatar.Image
              size={180}
              style={{ marginHorizontal: "auto" }}
              source={{ uri: inputs.image_data }}
            />
          </View>
        )}
        <Button
          mode="outlined"
          icon="image"
          style={styles.input}
          onPress={() => pickImage()}
        >
          {`${isEdit ? "Change" : "Add"} Profile Picture`}
        </Button>
        <Button
          mode="contained"
          style={styles.input}
          onPress={() => createProfile()}
        >
          {`${isEdit ? "Update" : "Create"} Profile`}
        </Button>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 24
  },
  input: {
    marginHorizontal: 24,
    marginVertical: 6
  }
});

export default ProfileForm;
