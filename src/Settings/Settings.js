import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet } from "react-native";
import { List, Avatar } from "react-native-paper";
import { firestore } from "react-native-firebase";
import { FlatList } from "react-native-gesture-handler";
import SearchBar from "../SearchBar";

const Settings = props => {
  const db = firestore().collection("notifications");
  const [notifications, setNoti] = useState([]);

  useEffect(() => {
    const unsubscribe = db.onSnapshot(snapshot => {
      snapshot.forEach(doc => {
        addNotifcation(doc.data());
      });
    });
    return unsubscribe;
  }, []);

  function addNotifcation(data) {
    setNoti(state => {
      return [...state, data];
    });
  }

  renderItem = ({ item }) => {
    console.log(item);

    return (
      <List.Item
        style={{ flex: 1 }}
        title={item.title}
        description={item.desc}
        left={() => <Avatar.Icon icon={item.icon} />}
      />
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ListHeaderComponent={
          <>
            <SearchBar />
            <List.Section>
              <List.Subheader>Notifications</List.Subheader>
            </List.Section>
          </>
        }
        ListEmptyComponent={
          <View style={styles.empty}>
            <Text style={{ fontSize: 20 }}>No notifications yet</Text>
          </View>
        }
        data={notifications}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default Settings;
