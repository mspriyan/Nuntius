import React, { useState } from "react";
import { ScrollView, View, Text, StyleSheet } from "react-native";
import { Button, TextInput } from "react-native-paper";
import AsyncStorage from "@react-native-community/async-storage";
import { auth, firestore } from "react-native-firebase";

const Auth = ({ navigation }) => {
  let db = firestore().collection("users");
  const [page, setPage] = useState({
    current: "phone",
    deactive: "otp"
  });
  const [otperr, setError] = useState(null);

  const [cred, setCred] = useState({
    phone: "+91",
    otp: "",
    confirmResult: null,
    user: null
  });

  const switchPage = () => {
    setPage(prev => ({
      current: prev.deactive,
      deactive: prev.current
    }));
  };

  const handleChange = (field, value) => {
    setCred({
      ...cred,
      [field]: value
    });
  };

  const saveToken = async data => {
    debugger;
    const token = data.toJSON();
    console.log(token);
    if (token) {
      const query = db.where("phoneNumber", "==", token.phoneNumber);
      const userData = await query.get();
      if (!userData.exist) {
        try {
          const userDoc = await db.doc(token.phoneNumber).set({
            name: "",
            bio: "",
            phoneNumber: token.phoneNumber
          });
          console.log(userDoc);
          AsyncStorage.setItem("TOKEN", token.phoneNumber).then(() =>
            navigation.navigate("ProfileForm")
          );
        } catch (e) {
          console.log(error);
        }
      } else {
        AsyncStorage.setItem("TOKEN", auth().currentUser.phoneNumber).then(() =>
          navigation.navigate("Home")
        );
      }
    }
  };

  // const action = () => {
  //   if (page.current === "Login") {
  //     tokenAuth();
  //   } else {
  //     createUser();
  //   }
  // };

  const action = () => {
    if (page.current === "phone" && cred.phone.length > 3) {
      auth()
        .signInWithPhoneNumber(cred.phone)
        .then(confirmResult => {
          handleChange("confirmResult", confirmResult);
          switchPage();
          console.log(confirmResult, "result");
          if (!confirmResult.verificationId) {
            navigation.navigate("AuthCheck");
          }
        })
        .catch(err => console.log(err));
    } else {
      cred.confirmResult
        .confirm(cred.otp)
        .then(user => saveToken(user))
        .catch(err => {
          console.log(err);
          setError("Please enter correct OTP");
        });
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Nuntius</Text>
      <View style={styles.loginCon}>
        <View style={styles.inputCon}>
          <TextInput
            mode="outlined"
            label="Phone No"
            value={cred.phone}
            disabled={page.current === "otp"}
            keyboardType="phone-pad"
            onChangeText={text => handleChange("phone", text)}
          />
        </View>
        <View style={styles.inputCon}>
          <TextInput
            mode="outlined"
            value={cred.otp}
            label="Enter OTP"
            error={otperr}
            disabled={page.current === "phone"}
            onChangeText={text => handleChange("otp", text)}
          />
        </View>
      </View>
      <View style={styles.buttonCon}>
        <Button
          mode="contained"
          // loading={loading || userLoading}
          color={page.current === "otp" && "green"}
          onPress={action}
        >
          {page.current === "phone" ? "Sign In" : "Verify"}
        </Button>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flex: 1,
    justifyContent: "center",
    margin: 16
  },
  title: {
    marginTop: 30,
    marginBottom: 40,
    textAlign: "center",
    fontSize: 36,
    color: "#3c5a99",
    fontWeight: "bold"
  },
  loginCon: {
    // flex: 2,
    justifyContent: "center"
  },
  inputCon: {
    marginHorizontal: 12,
    marginVertical: 6
  },
  buttonCon: {
    // flex: 1,
    justifyContent: "center",
    marginHorizontal: 12,
    marginBottom: 30,
    marginTop: 12
  }
});
export default Auth;
