import React, { Component } from "react";
import { ActivityIndicator } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { auth } from "react-native-firebase";

export default class AuthCheck extends Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    const { navigation } = this.props;
    let token;
    if (auth().currentUser) {
      token = auth().currentUser.phoneNumber;
      await AsyncStorage.setItem("TOKEN", token);
    }
    return navigation.navigate(token ? "App" : "AuthStack");
  }

  render() {
    return <ActivityIndicator />;
  }
}
