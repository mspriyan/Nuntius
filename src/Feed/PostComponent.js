import React from "react";
import { StyleSheet } from "react-native";
import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";

const NewPostComponent = props => {
  console.log(props);
  const { caption, user, post_image } = props.data;
  return (
    <Card>
      <Card.Title
        title={user}
        left={props => (
          <Avatar.Text size={36} label={user.substring(0, 2).toUpperCase()} />
        )}
      />
      <Card.Content>
        <Paragraph>{caption}</Paragraph>
      </Card.Content>
      <Card.Cover
        source={{ uri: post_image ? post_image : "https://picsum.photos/700" }}
      />
      <Card.Actions>
        <Button onPress={() => props.openModal(props.data.id)}>Comment</Button>
      </Card.Actions>
    </Card>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default NewPostComponent;
