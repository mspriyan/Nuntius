import React, { useEffect, useState } from "react";
import { FlatList } from "react-native";
import { firestore } from "react-native-firebase";
import { PostComponent } from ".";
import { Dimensions } from "react-native";
const { height: HEIGHT } = Dimensions.get("window");
function Feed(props) {
  const db = firestore().collection("posts");
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    getPosts(10);
  }, []);

  function getPosts(count) {
    count = count ? count : 10;
    db.limit(count)
      .get()
      .then(snapshot => {
        snapshot.forEach(doc => addPosts(doc.data(), doc.id));
      });
  }

  function addPosts(post, id) {
    console.log(post, id);
    post.id = id;
    setPosts(state => {
      return [...state, post];
    });
    console.log(posts);
  }

  const _keyExtractor = item => item.id;
  return (
    <FlatList
      data={posts}
      ListHeaderComponent={props.children}
      ListHeaderComponentStyle={{
        height: Math.abs(HEIGHT / 1.5)
      }}
      keyExtractor={_keyExtractor}
      renderItem={({ item }) => (
        <PostComponent
          data={item}
          username={props.username}
          openModal={props.openModal}
        />
      )}
      {...props}
    />
  );
}

export default Feed;
