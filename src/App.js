import React, { useEffect, useState } from "react";
import Routes from "./Routes";
import AsyncStorage from "@react-native-community/async-storage";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";

const theme = {
  ...DefaultTheme,
  roundness: 4,
  colors: {
    ...DefaultTheme.colors,
    primary: "#3c5a99",
    accent: "#fff"
  }
};

async function getToken() {
  const token = await AsyncStorage.getItem("TOKEN");
  return token;
}

export default function App() {
  return (
    <PaperProvider theme={theme}>
      <Routes />
    </PaperProvider>
  );
}
