import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import { FAB, Avatar } from "react-native-paper";
import { storage } from "react-native-firebase";
const { height, width } = Dimensions.get("screen");
import getKeyFromStorage from "../utils";

const NewHome = props => {
  const [image, setimage] = useState(null);
  useEffect(() => {
    let token;
    getKeyFromStorage("TOKEN").then(result => {
      token = result;
      if (token) {
        storage()
          .ref(`${token}/profile.jpg`)
          .getDownloadURL()
          .then(url => setimage({ uri: url }))
          .catch(err => console.log(err));
      }
    });
  }, []);

  function goToPost() {
    props.navigation.navigate("CreatePost");
  }

  return (
    <>
      <View style={styles.container}>
        <View style={styles.row}>
          <View>
            <FAB icon="add" onPress={goToPost} />
          </View>
        </View>
        <View style={styles.row1}>
          <View>
            <FAB icon="add" onPress={goToPost} />
          </View>
          <View>
            {image ? (
              <Avatar.Image size={120} source={image} />
            ) : (
              <Avatar.Icon size={120} icon="person" />
            )}
          </View>
          <View>
            <FAB icon="add" onPress={goToPost} />
          </View>
        </View>
        <View style={{ ...styles.row, ...styles.row2 }}>
          <View>
            <FAB icon="add" onPress={goToPost} />
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end"
  },
  row1: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly"
  },
  row2: {
    alignItems: "flex-start"
  }
});

export default NewHome;
