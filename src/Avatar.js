import React, { Component } from "react";
import { View, Image, StyleSheet } from "react-native";
import Images from "./config/images";
import AppStyles from "./config/styles";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Avatar extends Component {
  render() {
    const { uri, large, isGroup, enableDot } = this.props;

    return (
      <View style={large ? styles.avatarLargeView : styles.avatarView}>
        {isGroup ? (
          <Icon name="face" size={64} color="grey" />
        ) : (
          <Image
            source={uri ? { uri: uri } : Images.profile.avatar}
            style={large ? styles.avatarLarge : styles.avatar}
          />
        )}
        {enableDot ? (
          <View style={large ? styles.statusDotLarge : styles.statusDot} />
        ) : (
          <View style={{}} />
        )}
      </View>
    );
  }
}

Avatar.defultProps = {
  enableDot: true,
  large: false,
  isGroup: false,
  liveEnabled: true
};

const styles = StyleSheet.create({
  avatarView: {
    width: 48,
    height: 48,
    backgroundColor: AppStyles.colors.separator,
    borderRadius: 24
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    overflow: "hidden"
  },
  statusDot: {
    position: "absolute",
    bottom: 0,
    right: 0,
    width: 16,
    height: 16,
    borderRadius: 9,
    borderWidth: 2,
    borderColor: AppStyles.colors.white,
    backgroundColor: AppStyles.colors.onlineGreen
  },
  avatarLargeView: {
    width: 64,
    height: 64,
    backgroundColor: AppStyles.colors.separator,
    borderRadius: 32
  },
  avatarLarge: {
    width: 64,
    height: 64,
    borderRadius: 32,
    overflow: "hidden"
  },
  statusDotLarge: {
    position: "absolute",
    bottom: 0,
    right: 0,
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: AppStyles.colors.white,
    backgroundColor: AppStyles.colors.onlineGreen
  }
});
