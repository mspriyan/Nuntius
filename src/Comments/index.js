import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from "react-native";
import { TextInput, Modal, Divider, List } from "react-native-paper";
import { firestore } from "react-native-firebase";
import Icon from "react-native-vector-icons/FontAwesome";

const { height, width } = Dimensions.get("window");
const Comment = props => {
  const db = firestore().collection("comments");
  const [comments, setComments] = useState([]);
  const [loading, setLoading] = useState(false);
  const [text, setInputs] = useState("");

  useEffect(() => {
    const query = db.where("post_id", "==", props.post_id);
    const unsubscribe = query.onSnapshot(querySnapshot => {
      querySnapshot.forEach(doc => {
        addComments(doc.data());
        console.log(doc.data());
      });
    });
    return unsubscribe;
  }, []);

  function addComments(doc) {
    setComments(state => {
      return [...state, doc];
    });
  }

  function postComment() {
    if (text) {
      db.doc()
        .set({
          text,
          posted_by: props.data.user,
          post_id: props.post_id
        })
        .then(() => {
          setInputs("");
        });
    }
  }

  const _keyExtractor = (item, index) => index;

  function commentItem({ item }) {
    const isSameUser = item.posted_by.name === props.data.user.name;
    return (
      <List.Item
        title={isSameUser ? item.posted_by.name : "Anonymous"}
        description={item.text}
        left={props => <List.Icon {...props} icon="person" />}
      />
    );
  }

  return (
    <Modal
      visible={props.visible}
      onDismiss={props.handleClose}
      contentContainerStyle={styles.container}
    >
      <FlatList
        keyExtractor={_keyExtractor}
        contentContainerStyle={{ flex: 0.8, flexGrow: 0.8 }}
        data={comments}
        renderItem={item => commentItem(item)}
        ListEmptyComponent={
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ fontSize: 26 }}>No comments yet</Text>
          </View>
        }
      />
      <View style={{ flex: 0.1, flexGrow: 0.1 }}>
        <View style={styles.inputCon}>
          <View style={styles.input}>
            <TextInput
              mode="outlined"
              placeholder="write your comments"
              onChangeText={text => setInputs(text)}
            />
          </View>
          <TouchableOpacity onPress={postComment} style={styles.icon}>
            <Icon name="send" size={32} color={"#3c5a99"} />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12
  },
  inputCon: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  input: {
    flex: 0.8,
    justifyContent: "center",
    marginHorizontal: 8
  },
  icon: {
    flex: 0.2,
    justifyContent: "center",
    marginHorizontal: 12
  },
  commentCon: {
    marginHorizontal: 12,
    height: 25
  },
  comment: {
    fontSize: 16,
    margin: 4
  },
  user: {
    fontSize: 18,
    margin: 4
  }
});

export default Comment;
