import React from "react";
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator
} from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import Icons from "react-native-vector-icons/MaterialCommunityIcons";
import { Auth } from "./Auth";
import { Home } from "./Home";
import ActiveScreen from "./ActiveScreen";
import GroupScreen from "./GroupsScreen";
import { Starred } from "./Starred";
import { CreatePost } from "./Post";
import { Profile, ProfileForm } from "./Profile";
import { Settings } from "./Settings";
import AuthCheck from "./AuthCheck";

const TabIcons = ({ name, color }) => (
  <Icons size={26} name={name} style={{ color }} />
);

const AuthNav = createStackNavigator(
  {
    Auth: Auth
  },
  {
    headerMode: "none"
  }
);

const ProfileStack = createStackNavigator(
  {
    ProfilePage: Profile,
    ProfileForm,
    CreatePost: {
      screen: CreatePost
    },
    Feeling: Starred
  },
  {
    headerMode: "none",
    initialRouteName: "ProfilePage"
  }
);

const AppNav = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabIcons name={"home-outline"} color={tintColor} />
        )
      }
    },
    Notifications: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabIcons name="comment-outline" color={tintColor} />
        )
      }
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabIcons name="account-outline" color={tintColor} />
        )
      }
    },
    Users: {
      screen: ActiveScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabIcons name="account-group-outline" color={tintColor} />
        )
      }
    },
    Starred: {
      screen: GroupScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <TabIcons name="star-circle-outline" color={tintColor} />
        )
      }
    }
  },
  { initialRouteName: "Home" }
);

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthCheck: AuthCheck,
      App: AppNav,
      AuthStack: AuthNav
    },
    {
      initialRouteName: "AuthCheck"
    }
  )
);
