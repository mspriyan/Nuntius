import { StyleSheet } from "react-native";
import AppStyles from "../config/styles";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#f9f9f9"
  }
});

export default styles;
