import React, { Component } from "react";
import { FlatList } from "react-native";
import { Divider } from "react-native-paper";
import GroupsItem from "./GroupItem";
import styles from "./styles";

const data = [
  {
    name: "Awesome Profile Picture",
    commented: "15 days ago",
    members: "by Vicky"
  },
  {
    name: "Happy birthday",
    commented: "30 days ago",
    members: "by Alex"
  },
  {
    name: "Congratulations on New Job",
    commented: "30 days ago",
    members: "by Jacob"
  },
  {
    name: "Great Post",
    commented: "10 days ago",
    members: "by Bob"
  },
  {
    name: "Birthday Celebration",
    last_active: "5 days ago",
    members: "by Samuel"
  },
  {
    name: "College Buddies",
    last_active: "24 days ago",
    members: "by William"
  },
  {
    name: "Memories",
    last_active: "1 day ago",
    members: "by Tom"
  },
  {
    name: "Secret, Inside Joke uh",
    last_active: "28 days ago",
    members: "by Sam Tony"
  }
];

export default class GroupsList extends Component {
  renderItem = ({ item, index }) => {
    return <GroupsItem item={item} index={index} key={item.members} />;
  };

  render() {
    return (
      <FlatList
        data={data}
        contentContainerStyle={styles.list}
        ItemSeparatorComponent={() => <Divider />}
        renderItem={this.renderItem}
        showsVerticalScrollIndicator={false}
      />
    );
  }
}
