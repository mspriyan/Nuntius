import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Card, Text } from "react-native-paper";
import Icon from "react-native-vector-icons/Entypo";

import Avatar from "../../Avatar";
import styles from "./styles";

export default class GroupItem extends Component {
  onPress = () => {
    alert("Pressed");
  };
  render() {
    const { item, index } = this.props;
    return (
      <>
        <Card style={styles.card} onPress={this.onPress} elevation={3}>
          <Icon name="attachment" size={20} style={extra.pin} />
          <View style={styles.cardView}>
            <View style={styles.nameView}>
              {/* <Avatar large isGroup /> */}
              <Text style={styles.nameTag}>{`Comment #${index}`}</Text>
              <Text style={styles.nameText}>{item.name}</Text>
            </View>
            <View style={styles.footer}>
              <Text numberOflines={2} style={styles.members}>
                {item.members}
              </Text>
            </View>
          </View>
        </Card>
      </>
    );
  }
}

const extra = StyleSheet.create({
  pin: {
    position: "absolute",
    top: 0,
    left: 0,
    marginLeft: -4,
    marginTop: -4
  }
});
