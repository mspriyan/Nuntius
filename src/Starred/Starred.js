import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import SearchBar from "../SearchBar";
import { Searchbar } from "react-native-paper";

const Starred = props => {
  const [user, setUser] = useState("User");

  useEffect(() => {
    const user = props.navigation.getParam("user", false) || props.user;
    console.log(user);
    setUser(user);
  }, []);

  return (
    <>
      <SearchBar />
      <View style={styles.con}>
        <Text style={styles.test}>{`${user} is feeling awesome`}</Text>
        <Icon name="smile-o" size={48} />
      </View>
    </>
  );
};

Starred.defaultProps = {
  user: "John Doe"
};

const styles = StyleSheet.create({
  con: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontSize: 18,
    fontWeight: "bold",
    margin: 16
  }
});

export default Starred;
