import React, { useState, useEffect } from "react";
import { ScrollView, View, Text, StyleSheet, Image } from "react-native";
import ImagePicker from "react-native-image-picker";
import { firestore, storage } from "react-native-firebase";
import { TextInput, Button, Appbar } from "react-native-paper";

function CreatePost(props) {
  const db = firestore().collection("posts");
  const store = storage().ref();
  const [inputs, setInputs] = useState({
    caption: "",
    post_image: null,
    post_video: null,
    is_video: false,
    user: "",
    filename: ""
  });

  useEffect(() => {
    const user = props.navigation.getParam("user", {});
    handleChange("user", user.name);
  }, [props]);

  const [loading, setLoading] = useState(false);
  function handleChange(field, value) {
    setInputs({ ...inputs, [field]: value });
  }

  function pickImage(fileType) {
    let options;
    const imageOptions = {
      title: "Select Images",
      mediaType: "photo"
    };
    const videoOptions = {
      title: "Select a Video",
      mediaType: "video"
    };
    options = fileType === "photo" ? imageOptions : videoOptions;
    ImagePicker.launchImageLibrary(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        debugger;
        const uri = response.uri;
        const type = response.path.split(".").pop();
        console.log(type);
        if (type === "jpg") {
          setInputs(state => {
            return {
              ...state,
              post_image: uri,
              is_video: false
            };
          });
        } else {
          setInputs(state => {
            return {
              ...state,
              post_video: uri,
              is_video: true
            };
          });
        }
        console.log(type);
      }
    });
  }

  function addPost() {
    setLoading(true);
    const { post_image, post_video, is_video, ...rest } = inputs;
    db.add(rest)
      .then(docRef => {
        debugger;
        const fileType = is_video ? post_video : post_image;
        const fileName = is_video ? "video.mp4" : "image.jpg";
        store
          .child(`${inputs.user.phoneNumber}/${docRef.id}/${fileName}`)
          .putFile(fileType)
          .then(snapshot => {
            const field = is_video ? "post_video" : "post_image";
            db.doc(docRef.id)
              .set(
                {
                  [field]: snapshot.downloadURL
                },
                { merge: true }
              )
              .then(() => {
                setLoading(false);
                props.navigation.goBack();
              });
          });
      })
      .catch(err => {
        setLoading(false);
        console.log(err);
      });
  }

  return (
    <>
      <Appbar.Header style={styles.appBar}>
        <Appbar.BackAction onPress={() => props.navigation.goBack()} />
        <Appbar.Content title="Create Post" />
      </Appbar.Header>
      <ScrollView style={styles.container}>
        {/* <Text style={styles.title}>
          {inputs.user ? inputs.user : "Username"}
        </Text> */}
        <TextInput
          value={inputs.caption}
          mode="outlined"
          disabled={loading}
          multiline={true}
          numberOfLines={4}
          onChangeText={text => handleChange("caption", text)}
          autoFocus
          label="Write Something on your mind"
        />
        {inputs.post_image && (
          <Image
            style={{ width: 250, height: 300 }}
            source={{ uri: inputs.post_image }}
          />
        )}
        <Button
          mode="outlined"
          disabled={loading}
          icon="smile"
          style={styles.upBtn}
          onPress={() =>
            props.navigation.navigate("Feeling", { user: inputs.user })
          }
        >
          Feeling Activity
        </Button>
        <Button
          mode="outlined"
          disabled={loading}
          icon="image"
          style={styles.upBtn}
          onPress={() => pickImage("photo")}
        >
          Pick an Image
        </Button>
        <Button
          mode="outlined"
          disabled={loading}
          icon="video-library"
          style={styles.upBtn}
          onPress={() => pickImage("video")}
        >
          Upload a video
        </Button>
        <Button
          mode="contained"
          icon="add"
          onPress={addPost}
          loading={loading}
          style={styles.upBtn}
          disabled={!inputs.caption}
        >
          Create Post
        </Button>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 12
  },
  upBtn: {
    margin: 16
  },
  title: {
    margin: 16,
    fontSize: 24,
    fontWeight: "bold"
  },
  appBar: {
    color: "#3c5a99",
    backgroundColor: "white"
  }
});

export default CreatePost;
