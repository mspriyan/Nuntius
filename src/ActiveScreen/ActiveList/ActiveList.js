import React, { Component } from "react";
import { FlatList } from "react-native";
import { List, Avatar } from "react-native-paper";
import UserItem from "./UserItem";
import { users } from "./fake_data";
import SearchBar from "../../SearchBar";

export default class ActiveList extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log(this.props.navigation);
  }

  renderItem = ({ item }) => {
    return <UserItem item={item} />;
  };

  render() {
    return (
      <FlatList
        ListHeaderComponent={
          <>
            <SearchBar />
            <List.Section>
              <List.Subheader>Groups</List.Subheader>
              <List.Item
                title="Personal Group"
                left={() => <Avatar.Icon icon="group" />}
              />
              <List.Item
                title="Create Groups"
                left={() => <List.Icon color="#000" icon="group-add" />}
              />
            </List.Section>
            <List.Section>
              <List.Subheader>Contacts</List.Subheader>
            </List.Section>
          </>
        }
        data={users.results.slice(0, 15)}
        renderItem={this.renderItem}
        keyExtractor={item => item.login.uuid}
      />
    );
  }
}
