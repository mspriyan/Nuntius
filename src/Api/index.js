export { LOGIN_MUTATION, CREATE_USER, ME } from "./user";
export { ALL_POSTS, CREATE_POST } from "./post";
