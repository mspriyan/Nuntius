import gql from "graphql-tag";

export const LOGIN_MUTATION = gql`
  mutation Login($username: String!, $password: String!) {
    tokenAuth(username: $username, password: $password) {
      token
    }
  }
`;

export const CREATE_USER = gql`
  mutation Signup($input: UserInput!) {
    createUser(input: $input) {
      user {
        id
        username
      }
    }
  }
`;

export const ME = gql`
  query {
    me {
      id
      username
      nunuser {
        bio
        birthDate
        location
      }
    }
  }
`;
