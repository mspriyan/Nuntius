import gql from "graphql-tag";

export const ALL_POSTS = gql`
  query {
    allPosts {
      id
      title
      content
      createdDate
      postedBy {
        user {
          username
        }
      }
    }
  }
`;

export const CREATE_POST = gql`
  mutation newPost($input: PostInput, $file: Upload) {
    createPost(file: $file, input: $input) {
      post {
        id
        content
        title
      }
    }
  }
`;
