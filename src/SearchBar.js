import React, { useState } from "react";
import { View, Text } from "react-native";
import { Searchbar, IconButton, Menu } from "react-native-paper";
import { withNavigation } from "react-navigation";
import { auth } from "react-native-firebase";

const CustomSearchBar = props => {
  const [visible, setVisible] = useState(false);

  function showMenu() {
    setVisible(state => {
      return !state;
    });
  }

  return (
    <View
      style={{
        flexDirection: "row",
        width: "100%",
        alignItems: "center"
      }}
    >
      <Searchbar placeholder="Search" style={{ width: "80%", margin: 8 }} />
      <Menu
        visible={visible}
        onDismiss={showMenu}
        anchor={
          <IconButton icon="menu" color="blue" size={26} onPress={showMenu} />
        }
      >
        {props.user && props.profile_pic ? (
          <Menu.Item
            title="Account"
            onPress={() => {
              showMenu();
              props.navigation.navigate("ProfileForm", {
                user: props.user,
                profile_pic: props.profile_pic
              });
            }}
          />
        ) : null}
        <Menu.Item title="Invite a friend" />
        <Menu.Item title="Notification" />
        <Menu.Item title="Help" />
        <Menu.Item
          title="Logout"
          onPress={() =>
            auth()
              .signOut()
              .then(() => props.navigation.navigate("AuthStack"))
          }
        />
      </Menu>
    </View>
  );
};

export default withNavigation(CustomSearchBar);
